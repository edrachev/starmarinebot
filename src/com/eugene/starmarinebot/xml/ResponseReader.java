package com.eugene.starmarinebot.xml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.eugene.starmarinebot.Response;
import com.eugene.starmarinebot.galaxy.Galaxy;
import com.eugene.starmarinebot.galaxy.Planet;
import com.eugene.starmarinebot.galaxy.PlanetType;

public class ResponseReader {
	private static final String RESPONSE = "response";

	private static final String PLANETS = "planets";
	private static final String ERRORS = "errors";

	private static final String PLANET = "planet";
	private static final String ID = "id";
	private static final String OWNER = "owner";
	private static final String TYPE = "type";
	private static final String UNITS = "droids";
	private static final String NEIGHBOURS = "neighbours";
	private static final String NEIGHBOUR = "neighbour";

	private static final String ERROR = "error";

	public Response readGalaxy(InputStream input) throws ResponseReadException {
		Response result = null;
		XMLStreamReader reader = null;
		try {
			XMLInputFactory factory = XMLInputFactory.newFactory();
			reader = factory.createXMLStreamReader(input);

			reader.nextTag();

			if (RESPONSE.equals(reader.getName().getLocalPart())) {
				result = parseResponse(reader);
			} else {
				throw new ResponseReadException(RESPONSE + " element expected");
			}

			return result;
		} catch (XMLStreamException ex) {
			throw new ResponseReadException(ex);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (XMLStreamException ex) {
					// do nothing
				}
			}
		}
	}

	private Response parseResponse(XMLStreamReader reader) throws XMLStreamException, ResponseReadException {
		reader.nextTag();
		
		List<Planet> planets = new LinkedList<Planet>();
		if (PLANETS.equals(reader.getName().getLocalPart())) {
			Collection<Planet> loadlPlanets = parsePlanets(reader);
			planets.addAll(loadlPlanets);
		} else {
			throw new ResponseReadException(PLANETS + " element expected");
		}

		List<String> errors = new LinkedList<String>();
		if (ERRORS.equals(reader.getName().getLocalPart())) {
			Collection<String> localErrors = parseErrors(reader);
			errors.addAll(localErrors);
		} else {
			throw new ResponseReadException(ERRORS + " element expected");
		}
		return new Response(new Galaxy(planets), errors);
	}

	private Collection<Planet> parsePlanets(XMLStreamReader reader) throws XMLStreamException, ResponseReadException {
		Collection<Planet> planets = new ArrayList<Planet>();
		if (reader.nextTag() != XMLStreamReader.END_ELEMENT) {
			Map<String, Planet> planetsByID = new HashMap<String, Planet>();
			do {
				if (PLANET.equals(reader.getName().getLocalPart())) {
					planets.add(parsePlanet(reader, planetsByID));
				} else {
					throw new ResponseReadException(PLANET + " element expected");
				}
			} while (reader.isStartElement());
		}
		reader.nextTag();
		return planets;
	}

	private Planet parsePlanet(XMLStreamReader reader, Map<String, Planet> planetsByID) throws XMLStreamException, ResponseReadException {
		String planetId = reader.getAttributeValue(null, ID);
		Planet planet = getPlanetByID(planetsByID, planetId);

		reader.nextTag();
		planet.setOwner(getTagText(reader, OWNER));
		PlanetType type = parsePlanetType(getTagText(reader, TYPE));
		planet.setType(type);
		planet.setUnits(Integer.parseInt(getTagText(reader, UNITS)));

		if (NEIGHBOURS.equals(reader.getName().getLocalPart())) {
			parseNeighbours(reader, planetsByID, planet);
		} else {
			throw new ResponseReadException(NEIGHBOURS + " element expected");
		}
		reader.nextTag();
		return planet;
	}

	private void parseNeighbours(XMLStreamReader reader, Map<String, Planet> planetsByID, Planet planet) throws XMLStreamException, ResponseReadException {
		if (reader.nextTag() == XMLStreamReader.END_ELEMENT) {
			return;
		}

		do {
			String neighbourId = getTagText(reader, NEIGHBOUR);
			Planet neighbour = getPlanetByID(planetsByID, neighbourId);
			planet.addNeighbours(neighbour);
		} while (reader.isStartElement());
		reader.nextTag();
	}
	
	private Planet getPlanetByID(Map<String, Planet> planetsByID, String id) {
		Planet planet = planetsByID.get(id);
		if (planet == null) {
			planet = new Planet(id);
			planetsByID.put(id, planet);
		}
		return planet;
	}

	private Collection<String> parseErrors(XMLStreamReader reader) throws XMLStreamException, ResponseReadException {
		Collection<String> errors = new ArrayList<String>();
		if (reader.nextTag() != XMLStreamReader.END_ELEMENT) {
			
			do {
				if (ERROR.equals(reader.getName().getLocalPart())) {
					errors.add(getTagText(reader, ERROR));
				}
			} while (reader.isStartElement());
		}
		reader.nextTag();
		return errors;
	}

	private static String getTagText(XMLStreamReader reader, String tagName) throws XMLStreamException, ResponseReadException {
		String currentTagName = reader.getName().getLocalPart();

		if (reader.isStartElement() && tagName.equals(currentTagName)) {
			String textContent = reader.getElementText();
			reader.nextTag();
			return textContent;
		} else {
			throw new ResponseReadException(reader.getName().toString() + " found but " + tagName + " expected");
		}
	}

	private static PlanetType parsePlanetType(String s) {
		for (PlanetType type : PlanetType.values()) {
			if (type.name().equals(s)) {
				return type;
			}
		}
		throw new NoSuchElementException();
	}

}
