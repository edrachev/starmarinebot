package com.eugene.starmarinebot.xml;

public class ResponseReadException extends Exception {
	private static final long serialVersionUID = -5136544746686878539L;

	public ResponseReadException() {
		super();
	}

	public ResponseReadException(String message, Throwable cause) {
		super(message, cause);
	}

	public ResponseReadException(String message) {
		super(message);
	}

	public ResponseReadException(Throwable cause) {
		super(cause);
	}
}
