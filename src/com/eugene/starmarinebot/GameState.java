package com.eugene.starmarinebot;

public enum GameState {
	WAITING_START, CALCULATING, WAITING_ANSWER, FINISHED
}
