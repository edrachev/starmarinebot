package com.eugene.starmarinebot.galaxy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Planet {
	public static Comparator<Planet> sizeComparator = new Comparator<Planet>() {
		@Override
		public int compare(Planet o1, Planet o2) {
			return Integer.compare(o1.type.getLimit(), o2.type.getLimit());
		}
	};
	public static Comparator<Planet> poppulationComparator = new Comparator<Planet>() {
		@Override
		public int compare(Planet o1, Planet o2) {
			return Integer.compare(o1.units, o2.units);
		}
	};
	public static Comparator<Planet> poppulationToFullComparator = new Comparator<Planet>() {
		@Override
		public int compare(Planet o1, Planet o2) {
			return Integer.compare(o1.type.getLimit() - o1.units, o2.type.getLimit() - o2.units);
		}
	};
	
	private String id;
	private String owner;
	private int units;
	private int initialUnits;
	private PlanetType type;
	private List<Planet> neighbours = new ArrayList<Planet>();

	public Planet(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public int getUnits() {
		return units;
	}

	public int getInitialUnits() {
		return this.initialUnits;
	}
	
	public void setUnits(int units) {
		this.units = units;
		this.initialUnits = units;
	}

	public void addUnits(int units) {
		this.units += units;
	}
	
	public void substractUnits(int units) {
		this.units -= units;
	}
	
	public void substractInitialUnits(int units) {
		this.initialUnits -= units;
	}
	
	public PlanetType getType() {
		return type;
	}

	public void setType(PlanetType type) {
		this.type = type;
	}

	public List<Planet> getNeighbours() {
		return Collections.unmodifiableList(neighbours);
	}

	public void addNeighbours(Planet neighbour) {
		neighbours.add(neighbour);
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Planet neighbour : neighbours) {
			result.append(neighbour.getId()).append(", ");
		}

		return "Planet [id=" + id + ", owner=" + owner + ", Units=" + units + "/" + type.getLimit() + ", neighbours=" + result.toString() + "]";
	}

	@Override
	public boolean equals(Object obj) {
		return id.equals(((Planet)obj).id);
	}
	
	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
