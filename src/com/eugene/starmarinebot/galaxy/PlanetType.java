package com.eugene.starmarinebot.galaxy;

public enum PlanetType {
	TYPE_A(1.1, 100), TYPE_B(1.15, 200), TYPE_C(1.2, 500), TYPE_D(1.3, 1000);

	private final double increment;
	private final int limit;

	private PlanetType(double increment, int limit) {
		this.increment = increment;
		this.limit = limit;
	}

	public double getIncrement() {
		return increment;
	}
	public int getLimit() {
		return limit;
	}
}
