package com.eugene.starmarinebot.galaxy;

public class Move {

	private final Planet from;
	private final Planet to;
	private final int amount;

	public Move(Planet from, Planet to, int amount) {
		this.from = from;
		this.to = to;
		this.amount = amount;
	}

	public Planet getFrom() {
		return from;
	}

	public Planet getTo() {
		return to;
	}

	public int getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "from " + from.getId() + " to " + to.getId() + " -- " + amount;
	}
}
