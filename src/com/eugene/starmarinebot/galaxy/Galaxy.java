package com.eugene.starmarinebot.galaxy;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

public class Galaxy {
	private final Collection<Planet> planets;

	public Galaxy(Collection<Planet> planets) {
		this.planets = planets;
	}

	public Collection<Planet> getPlanets() {
		return planets;
	}
	
	@Override
	public String toString() {
		String message = "";
		Iterator<Planet> iterator = planets.iterator();
		SortedMap<String, Integer> numberUnits = new TreeMap<String, Integer>(); 
		while(iterator.hasNext()) {
			Planet planet = iterator.next();
			
			String owner = planet.getOwner();
			Integer number = numberUnits.get(owner);
			if(number == null) {
				number = 0;
			}
			number += planet.getUnits();
			numberUnits.put(owner, number);
			
			message += planet.toString();
			if(iterator.hasNext()) {
				message += '\n';
			}
		}
		
		message += '\n';
		
		for(Entry<String, Integer> entry: numberUnits.entrySet()) {
			message += entry.getKey() + " : " + entry.getValue() + '\n';
		}
		return message;
	}
}
