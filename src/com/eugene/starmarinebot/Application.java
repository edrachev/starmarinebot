package com.eugene.starmarinebot;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.Collections;

import com.eugene.starmarinebot.galaxy.Galaxy;
import com.eugene.starmarinebot.galaxy.Move;
import com.eugene.starmarinebot.strategy.PowerKnockStrategy;
import com.eugene.starmarinebot.strategy.Strategy;
import com.eugene.starmarinebot.xml.MovesWriteException;
import com.eugene.starmarinebot.xml.MovesWriter;
import com.eugene.starmarinebot.xml.ResponseReadException;
import com.eugene.starmarinebot.xml.ResponseReader;

public class Application {
	public static boolean debugging = false;
	
	private static Settings settings;
	private static Strategy strategy;
	
	public static void main(String[] args) {
		boolean gameStarted = false;
		settings = Settings.loadFromFile("settings.prop");
		strategy = new PowerKnockStrategy(settings.getName(), settings);
		
		ResponseReader reader = new ResponseReader();
		MovesWriter writer = new MovesWriter(settings.getToken());

		Collection<Move> moves = Collections.emptyList();
		Response response = null;
		
		System.out.print("Waiting connection");
		try {
			do {
				Socket socket = new Socket(settings.getServer(), settings.getPort());
				writer.writeMoves(new BufferedOutputStream(socket.getOutputStream()), moves);
				response = reader.readGalaxy(new BufferedInputStream(socket.getInputStream()));
				socket.close();

				if(gameStarted) {
					printErrors(response);
				} else {
					System.out.print(".");
				}
				
				if(response.isGameRunning()) {
					if(!gameStarted) {
						System.out.println("\nGame started\n");
					}
					gameStarted = true;
					Galaxy galaxy = response.getGalaxy();
					if(debugging) {
						System.out.println("Galaxy:\n" + galaxy.toString() + '\n');
					}
					
					moves = strategy.getMoves(galaxy);
					if(debugging) {
						printMoves(moves);
					}
					
				}
			} while (!gameStarted || response.isGameRunning());
			System.out.println("Game is finished!");
		} catch (IOException ex) {
			System.out.println("network porblem: " + ex.getMessage());
		} catch (MovesWriteException ex) {
			System.out.println("can not send request to server: " + ex.getMessage());
		} catch (ResponseReadException ex) {
			System.out.println("can not read server response: " + ex.getMessage());
		}
	}

	private static void printErrors(Response response) {
		for (String error : response.getErrors()) {
			System.out.println(error);
		}
	}
	
	private static void printMoves(Collection<Move> moves) {
		System.out.println("My moves:");
		for (Move move : moves) {
			System.out.println(move);
		}
		System.out.println();
	}
}
