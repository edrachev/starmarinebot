package com.eugene.starmarinebot.strategy;

import java.util.Collection;

import com.eugene.starmarinebot.galaxy.Galaxy;
import com.eugene.starmarinebot.galaxy.Move;

public interface Strategy {
	Collection<Move> getMoves(Galaxy galaxy);
}
