package com.eugene.starmarinebot.strategy;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;

import com.eugene.starmarinebot.Application;
import com.eugene.starmarinebot.Settings;
import com.eugene.starmarinebot.galaxy.Galaxy;
import com.eugene.starmarinebot.galaxy.Move;
import com.eugene.starmarinebot.galaxy.Planet;
import com.eugene.starmarinebot.galaxy.PlanetType;

public class PowerKnockStrategy implements Strategy {
	private static final Random random = new Random();
	private final String name;
	private final Settings settings;
	
	public PowerKnockStrategy(String name, Settings settings) {
		this.name = name;
		this.settings = settings;
	}

	private static Planet target;
	private static Map<Planet, Integer> tries = new HashMap<Planet, Integer>();
	private static Map<Planet, Integer> embargo  = new HashMap<Planet, Integer>();
	
	@Override
	public Collection<Move> getMoves(Galaxy galaxy) {
		Collection<Planet> planets = galaxy.getPlanets();
		if(planets.isEmpty()) {
			return Collections.emptyList();
		}
		
		Planet biggestPlanet = findBiggestPlanet(planets);
		Collection<Planet> myPlanets = planets
				.stream()
				.filter(p -> name.equals(p.getOwner()))
				.collect(Collectors.toList());
		List<Move> moves = new LinkedList<Move>();
			
		if(!hasSoBig(biggestPlanet, myPlanets)) {
			for(Planet planet: myPlanets) {
				moves.addAll(getMoveForRun1(planet));
			}
		}
		
		if(moves.isEmpty()) {
			Collection<Deque<Collection<Planet>>> planetStacks = getPlanetStacks(myPlanets);
			printPlanetStacks(planetStacks);
			for(Deque<Collection<Planet>> planetStack: planetStacks) {
				while(!planetStack.isEmpty()) {
					Collection<Planet> level = planetStack.pop();
					Collection<Planet> nextLevel = planetStack.peek();
					for(Planet planet: level) {
						if((myPlanets.size() == 1 && getFreeUnitsNextStep(planet) > 0) || (myPlanets.size() > 1)) {
							List<Planet> emptyPlanets = planet.getNeighbours().stream().filter(p -> isEmpty(p)).collect(Collectors.toList());
							Collections.sort(emptyPlanets, Planet.sizeComparator);
							Collections.reverse(emptyPlanets);
							for(Planet planetEl: emptyPlanets) {
								//int units = 2;
								int units = getMinRegenUnits(planetEl);
								if(planet.getInitialUnits() > units * 2) {
									moves.add(new Move(planet, planetEl, units));
									planetEl.addUnits(units);
									planet.substractInitialUnits(units);
								}
							}
						}
					
						int freeUnits = getFreeUnits(planet);
						if(Application.debugging) {
							System.out.println("freeUnits = " + freeUnits);
						}
						
						if(freeUnits > 0) {
							List<Planet> targetPlanets = nextLevel != null ? new LinkedList<>(getNeighboursFromNextLevel(planet, nextLevel)) : planet.getNeighbours();   
							addMovesForAttack(planet, targetPlanets, freeUnits, moves, nextLevel == null);
						    planet.substractUnits(freeUnits);
						}
					}
				}
			}
		}
		
		moves = processDuplicates(moves);
		
		return moves;
	}
	private void printPlanetStacks(Collection<Deque<Collection<Planet>>> planetStacks) {
		String message = "";
		for(Deque<Collection<Planet>> deque: planetStacks) {
			message += "\nDeque:\n";
			for(Collection<Planet> planets: deque) {
				message += "level: ";
				for(Planet p: planets) {
					message += p.getId();
					message += " ";
				}
				message += "\n";
			}
		}
		if(Application.debugging) {
			System.out.println(message);
		}
	}
	
	private int getMinRegenUnits(Planet planet) {
		return (int) Math.ceil(1 / (planet.getType().getIncrement() - 1));
	}
	
	private static class Pair<T> {
		private final T first;
		private final T second;
		public Pair(T first, T second) {
			this.first = first;
			this.second = second;
		}
		public T getFirst() {
			return first;
		}
		public T getSecond() {
			return second;
		}
		@Override
		public boolean equals(Object obj) {
			Pair<T> pair = (Pair<T>)obj; 
			return first.equals(pair.first) && second.equals(pair.second);
		}
		@Override
		public int hashCode() {
			return first.hashCode() + second.hashCode();
		}
	}
	
	private List<Move> processDuplicates(List<Move> moves) {
		Map<Pair<Planet>, Integer> map = new HashMap<>();
		for(Move move: moves) {
			Pair<Planet> pair = new Pair<>(move.getFrom(), move.getTo());
			Integer amount = map.get(pair);
			if(amount == null) {
				amount = 0;
			}
			amount += move.getAmount();
			map.put(pair, amount);
		}
		
		List<Move> result  = new LinkedList<Move>();
		for(Entry<Pair<Planet>, Integer> entry: map.entrySet()) {
			Pair<Planet> key = entry.getKey();
			result.add(new Move(key.getFirst(), key.getSecond(), entry.getValue()));
		}
		
		return result;
	}

	private void addMovesForAttack(Planet planet, Collection<Planet> planets, int transportingUnits, List<Move> moves, boolean lastLevel) {
		List<Collection<Planet>> planetGroups = getRangedPlanetsBySize(planets);
		for(Collection<Planet> group: planetGroups) {
			List<Planet> groupCopy = new LinkedList<Planet>(group);
			Collections.sort(groupCopy, Planet.poppulationComparator);
			
			List<Planet> emptyPlanets = groupCopy.stream().filter(p -> isEmpty(p)).collect(Collectors.toList());
			Collections.sort(emptyPlanets, new Comparator<Planet>() {
				@Override
				public int compare(Planet o1, Planet o2) {
					return random.nextInt(2) - 1;
				}
			});
			for(Planet planetEl: emptyPlanets) {
				int units = getNumberTransportingUnits(planetEl, transportingUnits);
				transportingUnits -= units;
				planetEl.addUnits(units);
				moves.add(new Move(planet, planetEl, units));
				if(transportingUnits <= 0) return;
			}
			
			for(Planet planetEl: groupCopy) {
				if(isMine(planetEl) && needMoreUnits(planetEl)) {
					int units = getNumberTransportingUnits(planetEl, transportingUnits);
					transportingUnits -= units;
					planetEl.addUnits(units);
					moves.add(new Move(planet, planetEl, units));
					if(transportingUnits <= 0) return;
				}
			}
			for(Planet planetEl: groupCopy) {
				if(!isMine(planetEl) && planetEl.getUnits() < transportingUnits) {
					int units = (int) (getNumberTransportingUnits(planetEl, transportingUnits));
					transportingUnits -= units;
					planetEl.addUnits(units);
					moves.add(new Move(planet, planetEl, units));
					if(transportingUnits <= 0) return;
				}
			}
		}
		
		if(lastLevel) {
			for(Collection<Planet> group: planetGroups) {
				for(Planet planetEl: group) {
					if(planetEl.equals(target)) {
						int units = (int) (getNumberTransportingUnits(planetEl, transportingUnits));
						transportingUnits -= units;
						planetEl.addUnits(units);
						moves.add(new Move(planet, planetEl, units));
						if(transportingUnits <= 0) return;
					}
				}
			}
		} else {
			int numPlanets = planets.size();
			int step = transportingUnits / numPlanets;
			for(Collection<Planet> group: planetGroups) {
				for(Planet planetEl: group) {
					int units = step;
					transportingUnits -= units;
					moves.add(new Move(planet, planetEl, units));
					planetEl.addUnits(units);
					if(transportingUnits <= 0) return;
				}
			}
		}
	}
	
	int getNumberTransportingUnits(Planet planet, int availbleUnits) {
		int needUnits = isMine(planet) ? (planet.getType().getLimit() - planet.getUnits()) : (planet.getType().getLimit() + planet.getUnits()); 
		if(needUnits > availbleUnits) {
			return availbleUnits;
		} else {
			return needUnits;
		}
	}
	
	private Collection<Planet> getNeighboursFromNextLevel(Planet planet, Collection<Planet> nextLevel) {
		List<Planet> result = new LinkedList<Planet>();
		for(Planet planetEl: planet.getNeighbours()) {
			if(nextLevel.contains(planetEl)) {
				result.add(planetEl);
			}
		}
		return result;
	}

	private Planet findBiggestPlanet(Collection<Planet> planets) {
		return Collections.max(planets, Planet.sizeComparator);
	}
	private boolean isMine(Planet planet) {
		return name.equals(planet.getOwner());
	}
	private boolean isEmpty(Planet planet) {
		return planet.getUnits() == 0;
	}
	private int getFreeUnits(Planet planet) {
		int effectivePopulation = getEffectivePopulation(planet);
		int units = planet.getUnits() - effectivePopulation;
		if(units > planet.getInitialUnits()) {
			return planet.getInitialUnits();
		}
		return units > 0 ? units : 0;
	}
	
	private int getFreeUnitsNextStep(Planet planet) {
		int effectivePopulation = getEffectivePopulation(planet);
		int units = (int) (planet.getUnits() * planet.getType().getIncrement());
		units = units - effectivePopulation;
		if(units > planet.getInitialUnits()) {
			return planet.getInitialUnits();
		}
		return units > 0 ? units : 0;
	}

	private int getEffectivePopulation(Planet planet) {
		PlanetType type = planet.getType();
		int effectivePopulation = (int)(type.getLimit() /  type.getIncrement());
		return effectivePopulation;
	}
	
	private boolean needMoreUnits(Planet planet) {
		return getEffectivePopulation(planet) > planet.getUnits();
	}
	
	private List<Collection<Planet>> getRangedPlanetsBySize(Collection<Planet> planets) {
		List<Collection<Planet>> result = new LinkedList<>();
		result.add(getPlanetsByType(planets, PlanetType.TYPE_D));
		result.add(getPlanetsByType(planets, PlanetType.TYPE_C));
		result.add(getPlanetsByType(planets, PlanetType.TYPE_B));
		result.add(getPlanetsByType(planets, PlanetType.TYPE_A));
		return result;
	}
	
	private Collection<Planet> getPlanetsByType(Collection<Planet> planets, PlanetType planetType) {
		List<Planet> result = new LinkedList<Planet>();
		for(Planet planet: planets) {
			if(planet.getType().equals(planetType)) {
				result.add(planet);
			}
		}
		return result;
	}
	
	private boolean hasSoBig(Planet planet, Collection<Planet> planets) {
		int limit = planet.getType().getLimit();
		for(Planet planetEl: planets) {
			if(planetEl.getType().getLimit() == limit && planetEl.getUnits() > 500) {
				return true;
			}
		}
		return false;
	}
	private Collection<Move> getMoveForRun(Planet planet) {
		Planet biggest = Collections.max(planet.getNeighbours(), Planet.sizeComparator);
		if(biggest.getType().getLimit() > planet.getType().getLimit()) {
			return Collections.singletonList(new Move(planet, biggest, planet.getUnits()));
		}
		return Collections.emptyList();
	}
	
	private Collection<Move> getMoveForRun1(Planet planet) {
		List<Move> moves = new LinkedList<Move>();
		for(Planet p: planet.getNeighbours()) {
			if(isMine(p) && p.getType().getLimit() > planet.getType().getLimit() && p.getUnits() < p.getType().getLimit() * 0.5) {
				moves.add(new Move(planet, p, planet.getUnits()));
				return moves;
			}
		}
		for(Planet p: planet.getNeighbours()) {
			if(p.getType().getLimit() > planet.getType().getLimit()) {
				if(!isEmpty(p) && p.getType().equals(PlanetType.TYPE_D) && ((p.getUnits() < planet.getUnits()) || isMine(p))) {
					moves.add(new Move(planet, p, planet.getUnits()));
					return moves;
				}
				if(isEmpty(p)) {
					int freeUnits = getFreeUnits(planet);
					planet.substractUnits(freeUnits);
					moves.add(new Move(planet, p, freeUnits > 0 ? freeUnits : 1));
				} else if (p.getUnits() * 4 < planet.getUnits()) {
					moves.add(new Move(planet, p, planet.getUnits()));
					return moves;
				}
			}
		}
		return moves;
	}
	
	private Collection<Deque<Collection<Planet>>> getPlanetStacks(Collection<Planet> planets) {
		Collection<Planet> planetsCopy = new LinkedList<>(planets);
		Collection<Deque<Collection<Planet>>> result = new LinkedList<>();
		while(!planetsCopy.isEmpty()) {
			result.add(getPlanetsByLevels(planetsCopy));
		}
		return result;
	}
	
	private Deque<Collection<Planet>> getPlanetsByLevels(Collection<Planet> planets) {
		Deque<Collection<Planet>> result = new LinkedList<>();
		Collection<Planet> previousLevel = getFirstLevel(planets);
		result.push(previousLevel);
		
		while(!planets.isEmpty() && !previousLevel.isEmpty()) {
			previousLevel = getNextLevel(planets, previousLevel);
			if(!previousLevel.isEmpty()) {
				result.push(previousLevel);
			}
		}
		return result;
	}
	
	private Collection<Planet> getFirstLevel(Collection<Planet> planets) {
		List<Planet> enemyPlanets = new LinkedList<Planet>();
		for(Planet myPlanet: planets) {
			for(Planet planetEl: myPlanet.getNeighbours()) {
				if(!isMine(planetEl)) {
					enemyPlanets.add(planetEl);
				}
			}
		}
		
		target = getEnemyPlanetForAttack(enemyPlanets);
		
		List<Planet> result = new LinkedList<Planet>();
		Iterator<Planet> iterator = planets.iterator();
		while(iterator.hasNext()) {
			Planet planet = iterator.next();
			for(Planet neighbour: planet.getNeighbours()) {
				if(neighbour.equals(target)) {
					result.add(planet);
					iterator.remove();
					break;
				}
			}
		}
		return result;
	}
	
	private Planet getEnemyPlanetForAttack(List<Planet> enemyPlanets) {
		Map<Planet, Double> map = new HashMap<Planet, Double>();
		for(Planet planet: enemyPlanets) {
			int freeUnits = 0;
			double inc = 1;
			double enemyFreeUnits = 0;
			double potentiallyFreeUnits = 0;
			for(Planet neighbour: planet.getNeighbours()) {
				if(isMine(neighbour)) {
					double tmp_inc = neighbour.getType().getIncrement();
					if(tmp_inc > inc) {
						inc = tmp_inc;
					}
					int tmp = getFreeUnits(neighbour);
					if(tmp > freeUnits) {
						freeUnits = tmp;
					}
					for(Planet myP: neighbour.getNeighbours()) {
						if(isMine(myP)) {
							potentiallyFreeUnits += getFreeUnits(myP);
						}
					}
				} else if(planet.getOwner().equals(neighbour.getOwner())) {
					enemyFreeUnits += getFreeUnits(neighbour);
				}
			}
			double k = planet.getType().getIncrement() * inc * 1000 + ((freeUnits + potentiallyFreeUnits)/ ((planet.getUnits() + 1 + enemyFreeUnits)));
			map.put(planet, k);
		}
		
		List<Entry<Planet, Double>> list = new LinkedList<>(map.entrySet());
		Collections.sort(list, new Comparator<Entry<Planet, Double>>() {
			@Override
			public int compare(Entry<Planet, Double> o1,
					Entry<Planet, Double> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}	
		});
		Collections.reverse(list);
		for(Entry<Planet, Double> entry: list) {
			Planet pl = processPlanetWithEmbargo(entry);
			if(pl != null) {
				return pl;
			}
		}
		return list.get(0).getKey();
	}
	private Planet processPlanetWithEmbargo(Entry<Planet, Double> entry) {
		Planet pl = entry.getKey();
		Integer embargoValue = embargo.get(pl);
		if(embargoValue != null) {
			embargoValue--;
			embargoValue = embargoValue == 0 ? null : embargoValue;
			embargo.put(pl, embargoValue);
		} else {
			Integer tryValue = tries.get(pl);
			if(tryValue == null) {
				tryValue = 0;
			} else {
				tryValue++;
			}
			tries.put(pl, tryValue);
			if(tryValue == 7) {
				tries.remove(pl);
				embargo.put(pl, 7);
			} else {
				return pl;
			}
		}
		return null;
	}
	
	private Collection<Planet> getNextLevel(Collection<Planet> planets, Collection<Planet> previousLevel) {
		List<Planet> neighbours = new LinkedList<Planet>();
		for(Planet planet: previousLevel) {
			neighbours.addAll(planet.getNeighbours());
		}
		
		List<Planet> result = new LinkedList<Planet>();
		Iterator<Planet> iterator = planets.iterator();
		while (iterator.hasNext()) {
			Planet planet = iterator.next();
			if(neighbours.contains(planet)) {
				result.add(planet);
				iterator.remove();
			}
		}
		return result;
	}
}
