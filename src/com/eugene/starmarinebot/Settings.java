package com.eugene.starmarinebot;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Settings {
	private final String server;
	private final int port;
	private final String name;
	private final String token;
	
	public Settings(String server, int port, String name, String token) {
		this.server = server;
		this.port = port;
		this.name = name;
		this.token = token;
	}

	public static Settings loadFromFile(String fileName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(fileName);
			Properties props = new Properties();
			props.load(file);

			return new Settings(props.getProperty("server"), Integer.parseInt(props.getProperty("port")), props.getProperty("name"), props.getProperty("token"));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (file != null) {
				try {
					file.close();
				} catch (IOException ex) {
				}
			}
		}
	}
	
	public String getServer() {
		return server;
	}
	public int getPort() {
		return port;
	}
	public String getName() {
		return name;
	}
	public String getToken() {
		return token;
	}
}
