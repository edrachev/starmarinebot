package com.eugene.starmarinebot;

import java.util.Collection;

import com.eugene.starmarinebot.galaxy.Galaxy;

public class Response {
	private final Galaxy galaxy;
	private final Collection<String> errors;

	public Response(Galaxy galaxy, Collection<String> errors) {
		this.galaxy = galaxy;
		this.errors = errors;
	}

	public Galaxy getGalaxy() {
		return galaxy;
	}

	public Collection<String> getErrors() {
		return errors;
	}

	public boolean isGameRunning() {
		boolean result = true;
		for (String error : errors) {
			if (error.startsWith("Game is finished. ") || error.startsWith("User has not joined any game")) {
				result = false;
			}
		}
		return result;
	}

}
